let to_do_task = [];
let completed_task = [];
const BASE_URL = "https://62f8b7483eab3503d1da151c.mockapi.io";

//add task to do
document.querySelector("#addItem").addEventListener("click", () => {
  let task_name = document.querySelector("#newTask").value.trim();
  if (task_name === "") {
    return;
  }
  let new_task = new Task(task_name, "incompleted");
  axios({
    url: `${BASE_URL}/User_Manager`,
    method: "post",
    data: new_task,
  })
    .then(function () {
      renderTaskService();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
  document.querySelector("#newTask").value = "";
});
// show task done
document.querySelector("#one").addEventListener("click", () => {
  document.querySelector("#todo").classList.add("d-none");
});
// sort a-z
document.querySelector("#two").addEventListener("click", () => {
  sortAtoZ(to_do_task, "todo");
  sortAtoZ(completed_task, "completed");
});
//sort z-a
document.querySelector("#three").addEventListener("click", () => {
  sortZtoA(to_do_task, "todo");
  sortZtoA(completed_task, "completed");
});
//show all task
document.querySelector("#all").addEventListener("click", () => {
  document.querySelector("#todo").classList.remove("d-none");
});
let sortAtoZ = (task, id) => {
  let array_name = [];
  task.forEach((item) => {
    array_name.push(item.name);
  });
  let arranged_array_name = array_name.sort((a, b) => a.localeCompare(b));
  let arranged_task = [];
  arranged_array_name.forEach((name, index) => {
    arranged_task[index] = task.find((item) => {
      return item.name === name;
    });
  });
  renderTask(arranged_task, id);
};
let sortZtoA = (task, id) => {
  let array_name = [];
  task.forEach((item) => {
    array_name.push(item.name);
  });
  let arranged_array_name = array_name.sort((a, b) => b.localeCompare(a));
  let arranged_task = [];
  arranged_array_name.forEach((name, index) => {
    arranged_task[index] = task.find((item) => {
      return item.name === name;
    });
  });
  renderTask(arranged_task, id);
};
let removeTask = (id) => {
  axios({
    url: `${BASE_URL}/User_Manager/${id}`,
    method: "delete",
  })
    .then(function () {
      renderTaskService();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
};
// complete, uncomplete task
let checkTask = (id) => {
  let task_update = to_do_task.find((item) => {
    return item.id === id;
  });
  if (task_update === undefined) {
    task_update = task_update = completed_task.find((item) => {
      return item.id === id;
    });
  }
  task_update.status =
    task_update.status == "completed" ? "incompleted" : "completed";
  axios({
    url: `${BASE_URL}/User_Manager/${id}`,
    method: "put",
    data: task_update,
  })
    .then(function () {
      renderTaskService();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
};

let renderTask = (list, id) => {
  let contentUL = ``;
  list.forEach((item) => {
    contentUL += `
    <li>
    ${item.name}
    <div class="d-flex justify-content-center align-items-center">
      <button class="btn remove" onclick = "removeTask('${item.id}')">
        <i class="fa-solid fa-trash"></i>
      </button>
      <button class="btn complete ml-2 " onclick = "checkTask('${item.id}')">
        <i class="fa-solid fa-circle-check"></i>
      </button>
    </div>
  </li>
    `;
  });
  document.getElementById(id).innerHTML = contentUL;
};
const renderTaskService = () => {
  to_do_task = [];
  completed_task = [];
  axios({
    url: `${BASE_URL}/User_Manager`,
    method: "GET",
  })
    .then(function (response) {
      let data = response.data;
      data.forEach((item) => {
        if (item.status === "incompleted") {
          to_do_task.push(item);
        } else {
          completed_task.push(item);
        }
      });
      renderTask(to_do_task, "todo");
      renderTask(completed_task, "completed");
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
renderTaskService();
